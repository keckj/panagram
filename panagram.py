#!/usr/bin/env python2.7

import os
import itertools as it

def load_data(path, start='', end=''):
    if not os.path.isfile(path):
        msg="File '{}' does not exist !"
        raise RuntimeError(msg.format(path))
    with open(path) as f:
        words = f.readlines()
    words = map(str.strip, words)
    print '------------------------------------------------------------'
    print "Loaded {} words from dictionary '{}'.".format(len(words), path)
    if (start not in ('',None)):
        words = filter(lambda x: x.startswith(start), words)
    if (end not in ('',None)):
        words = filter(lambda x: x.endswith(end), words)
    words = set(words)
    nwords = len(words)
    print '------------------------------------------------------------'
    print "{} words left after start='{}' and end='{}' constraints.".format(nwords, start, end)
    if (nwords<20):
        for word in words:
            print word
    return words


def anagrams(dictionary, candidates, start=''):
    letters = tuple(k      for k in candidates.keys())
    counts  = tuple(len(v) for v in candidates.values())

    if start in ('', None):
        start_words = ()
        nstart_words = 0
        wstart = ''
    else:
        start_words=tuple(start.split(' '))
        nstart_words=len(start_words)
        wstart = ''
        for word in start_words:
            c = word[0].lower()
            assert c in candidates, word
            assert word in candidates[c], word
            wstart += c

    words = load_data(dictionary, start=wstart)
    nwords = len(words)
    npermutations = 2**sum(counts)
    print '------------------------------------------------------------'
    print 'Letters are:        {}'.format(letters)
    print 'Letter counts are:  {}'.format(counts)
    print 'Number of possible permutations: {}'.format(npermutations)
    print '------------------------------------------------------------'

    print 'Looking for anagrams in a database of {} words...'.format(nwords)
    print 'Pre-filtering dictionary with letters...'
    words = filter(lambda word: all((c in letters) for c in word), words)

    print 'Filtering with letter counts...'
    words = filter(lambda word: all((word.count(c) <= len(candidates[c])) for c in word), words)
    words = sorted(words, key=str.__len__, reverse=True)
    nwords = len(words)

    print '------------------------------------------------------------'
    print '{} words matched:'.format(nwords)
    for word in words:
        print word
    print '------------------------------------------------------------'
    for word in words:
        print '::{}::'.format(word.upper())
        word_candidates = tuple(candidates[c] for c in word[nstart_words:])
        word_candidates = it.product(*word_candidates)
        word_candidates = it.ifilter(lambda x: len(start_words+x)==len(set(start_words+x)), word_candidates)
        for wc in word_candidates:
            print '{} {}'.format(start, ' '.join(wc))
        print



if __name__ == '__main__':

    dictionary='dictionary.txt'

    candidates = {
        'a': {'Annuel','Agent'},
        'b': {'Boisson','Brevage'},
        'c': {'Commite', 'Chaud','Cafe'},
        'd': {'Debit'},
        'e': {'Energisant', 'Expresso'},
        'g': {'Grand','Gratuit','Grain'},
        'h': {'Hydratation'},
        'i': {'Investissement','Infusion'},
        'm': {'Multisite'},
        'p': {'Plan','Pour','Plante', 'Psychotrope'},
        'r': {'Reflexion'},
        's': {'Stimulant'},
        't': {'The', 'Table-ronde', 'Travail'},
    }

    anagrams(dictionary, candidates, start='Plan Investissement Multisite')

